#include "mainwindow.h"
#include <QApplication>
#include "plansza.h"
#include <QStatusBar>

int main(int argc, char *argv[])
{

    QApplication a(argc, argv);
    MainWindow w;

    w.statusBar()->setSizeGripEnabled( false );;
    w.setFixedSize(w.size());
    w.show();

    return a.exec();

}
