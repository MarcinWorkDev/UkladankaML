#include "planszaview.h"
#include "puzelview.h"
#include <QMessageBox>
#include "ccdelay.h"

PlanszaView::PlanszaView(int x, int y, int level)
{
    this->x = x;
    this->y = y;
    this->level = level;

    this->buildLayout();
}

void PlanszaView::buildLayout()
{

    Plansza plansza = Plansza(this->x,this->y,this->level);
    this->plansza = plansza;
    this->draw();

}

void PlanszaView::draw()
{
    for (auto i=0 ; i<plansza.puzzles.size() ; ++i)
    {
        for (auto ii=0 ; ii<plansza.puzzles[i].size() ; ++ii)
        {
            Puzel puzel = plansza.puzzles[i][ii];
            if (puzel.isEmpty)
            {
                this->addWidget(new PuzelView(), i, ii);
            } else {
                this->addWidget(new PuzelView(puzel.v, puzel.isCorrect), i, ii);
            }
        }
    }

    this->findBlank();
}

void PlanszaView::redraw()
{

    for(int idx = 0; idx < this->count(); idx++)
    {
        QLayoutItem * const item = this->itemAt(idx);
        if(dynamic_cast<QWidgetItem *>(item))
            delete item->widget();
    }

    this->draw();
}

void PlanszaView::findBlank()
{

    for (auto i=0 ; i<plansza.puzzles.size() ; ++i)
    {
        for (auto ii=0 ; ii<plansza.puzzles[i].size() ; ++ii)
        {
            Puzel puzel = plansza.puzzles[i][ii];
            if (puzel.isEmpty)
            {
                this->blankX = ii;
                this->blankY = i;
            }
        }
    }

}

void PlanszaView::move(Qt::Key key)
{

    // sprawdzenie ktory bloczek do ruchu w zaleznosci od nacisnietego klawisza
    switch (key)
    {
    case Qt::Key_Left:
        plansza.movePuzel(blankX+1, blankY, blankX, blankY, true);
        break;
    case Qt::Key_Right:
        plansza.movePuzel(blankX-1, blankY, blankX, blankY, true);
        break;
    case Qt::Key_Up:
        plansza.movePuzel(blankX, blankY+1, blankX, blankY, true);
        break;
    case Qt::Key_Down:
        plansza.movePuzel(blankX, blankY-1, blankX, blankY, true);
        break;
    default:
        break;
    }

    this->redraw();
    this->findBlank();

    if (this->checkGame())
    {
        QMessageBox msgBox;
        msgBox.setText("Udało się! Układ został rozwiązany! :)");
        msgBox.exec();
    }

}

void PlanszaView::undoMove()
{
    plansza.undoMove();
    this->redraw();
    this->findBlank();

    if (this->checkGame())
    {
        QMessageBox msgBox;
        msgBox.setText("Udało się! Układ został rozwiązany! :)");
        msgBox.exec();
    }
}

bool PlanszaView::checkGame()
{

    for (auto i=0 ; i<plansza.puzzles.size() ; ++i)
    {
        for (auto ii=0 ; ii<plansza.puzzles[i].size() ; ++ii)
        {
            Puzel puzel = plansza.puzzles[i][ii];
            if (!puzel.isCorrect)
            {
               return false;
            }
        }
    }

    return true;
}

void PlanszaView::solve()
{
    while (!this->checkGame())
    {
        ccdelay<int>().setDelay(100);
        this->undoMove();
    }
}
