#include "puzelview.h"

PuzelView::PuzelView()
{
    this->setDigitCount(0);
    this->display(0);
    this->setStyleSheet("background-color: white;");
}

PuzelView::PuzelView(int v, bool isCorrect)
{
    this->setDigitCount(2);
    this->display(v);
    this->setStatus(isCorrect);
}

void PuzelView::setStatus(bool isCorrect){
    if (isCorrect)
    {
        this->setStyleSheet("background-color: green;");
    } else {
        this->setStyleSheet("background-color: red;");
    }
}
