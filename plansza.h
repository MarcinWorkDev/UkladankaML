#ifndef PLANSZA_H
#define PLANSZA_H

#include <vector>
#include "puzel.h"
#include <iostream>

/**
 *  Klasa odpowiedzialna za logikę planszy do gry. Klasa odpowiada za utworzenie obiektów puzli oraz ich przemieszczanie w obrębie planszy.
 */
class Plansza
{
public:
    /**
     *  Kontener puzli składających się na układankę.
     */
    std::vector< std::vector<Puzel> > puzzles; // lista slotow [posY][posX] = PUZEL albo null
    /**
     *  Pusty konstruktor bez określonych atrybutów początkowych.
     */
    Plansza();
    /**
     *  Konstruktor określający wybiary planszy.
     * @param x szerokość planszy (liczba elementów w osi X)
     * @param y wysokość planszy (liczba elemtnów w osi Y)
     * @param level poziom trudności układanki
     */
    Plansza(int x, int y, int level);
    /**
     *  Metoda powodująca przesunięcie puzla z pozycji 'from' na pozycję 'to'. Przesunięcie określone jest jako przesunięcie manualne użytkownika.
     * @param fromX współrzędna X pozycji aktualnej puzla
     * @param fromY współrzędna Y pozycji aktualnej puzla
     * @param toX współrzędna X pozycji docelowej puzla
     * @param toY wpsółrzędna Y pozycji docelowej puzla
     * @return bool true jeżeli ruch został wykonany lub false jeżeli ruch nie był prawidłowy
     */
    bool movePuzel(int fromX, int fromY, int toX, int toY);
    /**
     *  Metoda powodująca przesunięcie puzla z pozycji 'from' na pozycję 'to'. Przesunięcie określone jest jako przesunięcie manualne użytkownika.
     * @param fromX współrzędna X pozycji aktualnej puzla
     * @param fromY współrzędna Y pozycji aktualnej puzla
     * @param toX współrzędna X pozycji docelowej puzla
     * @param toY wpsółrzędna Y pozycji docelowej puzla
     * @param isUserMove określa czy ruch jest wykonywany przez komputer (losowanie) czy przez użytkownika
     * @return bool true jeżeli ruch został wykonany lub false jeżeli ruch nie był prawidłowy
     */
    bool movePuzel(int fromX, int fromY, int toX, int toY, bool isUserMove);
    /**
     *  Cofnięcie jednego ruchu na planszy
     */
    void undoMove();
    /**
     *  Cofnięcie ruchu na planszy
     * @param isUserMove określa czy ruch jest wykonany przez komputer (losowanie) czy przez użytkownika
     * @param moves określa liczbę ruchów ile ma zostać cofniętych
     */
    void undoMove(bool isUserMove, int moves);
    /**
     *  Metoda wyświetla aktualny układ planszy w konsoli
     */
    void show();
    /**
     *  Metoda automatycznie rozwiązuje układ (bez wizualizacji przesunięć)
     */
    void solveFast();


protected:
    int x; // rozmiar planszy x
    int y; // rozmiar planszy y
    int level; // poziom 1 - latwy, 2 - sredni, 3 - trudny

private:
    void build();
    void random();
    struct moveData{
        int fromX;
        int fromY;
        int toX;
        int toY;
        bool isUserMove;

        bool operator==(moveData md){
            if (md.fromX == fromX && md.fromY == fromY && md.toX == toX && md.toY == toY){
                return true;
            } else {
                return false;
            }
        }
    };
    std::vector< moveData > movesHistory;
    bool validMove(moveData);

};

#endif // PLANSZA_H
