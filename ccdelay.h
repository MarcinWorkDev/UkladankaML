#ifndef CCDELAY_H
#define CCDELAY_H

#include <QTime>
#include <QCoreApplication>

/**
 *  Klasa powodująca wstrzymanie logiki aplikacji na określony czas.
 */
template<class T>
class ccdelay
{
public:
    ccdelay(){}
    /**
     *  Metoda wywołuje wstrzymanie aplikacji na określony czas
     * @param millisecondsToWait liczba milisekund na jaki wstrzymane zostanie działanie aplikacji
     */
    void setDelay( T millisecondsToWait )
    {
        millisecondsToWait = (int)millisecondsToWait;

        QTime dieTime = QTime::currentTime().addMSecs( millisecondsToWait );
        while( QTime::currentTime() < dieTime )
        {
            QCoreApplication::processEvents( QEventLoop::AllEvents, 100 );
        }

    }
};

#endif // CCDELAY_H
