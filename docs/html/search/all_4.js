var searchData=
[
  ['plansza',['Plansza',['../class_plansza.html',1,'Plansza'],['../class_plansza_view.html#abd4aca272f3dfd6373347b233fa26ce2',1,'PlanszaView::plansza()'],['../class_plansza.html#ae58ab9e9e0a12c39f36750a0faa2c100',1,'Plansza::Plansza()'],['../class_plansza.html#aa62c16209d2f269b064f36a670540606',1,'Plansza::Plansza(int x, int y, int level)']]],
  ['planszaview',['PlanszaView',['../class_plansza_view.html',1,'PlanszaView'],['../class_plansza_view.html#adbd550a8e6dbec1e3f50c408c7ce4222',1,'PlanszaView::PlanszaView()']]],
  ['puzel',['Puzel',['../class_puzel.html',1,'Puzel'],['../class_puzel.html#a204ea9a423d1d305fbd6cc1d1928af16',1,'Puzel::Puzel()']]],
  ['puzelview',['PuzelView',['../class_puzel_view.html',1,'PuzelView'],['../class_puzel_view.html#a0c881dd9820367aa39232fb115d3f210',1,'PuzelView::PuzelView()'],['../class_puzel_view.html#a8cc651227e9e38867c63662aa81b1b3b',1,'PuzelView::PuzelView(int v, bool isCorrect)']]],
  ['puzzles',['puzzles',['../class_plansza.html#a2c2e0f0c15a7f1736ee728b3b4ccf8b5',1,'Plansza']]]
];
