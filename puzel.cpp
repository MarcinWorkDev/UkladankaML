#include "puzel.h"

Puzel::Puzel(int ax, int ay, int v)
{
    this->cx = ax;
    this->cy = ay;
    this->v = v;

    this->setCorrect(true);
    this->setEmpty(false);
}

void Puzel::setCorrect(bool correct){
    this->isCorrect = correct;
}

void Puzel::setEmpty(bool empty){
    this->isEmpty = empty;
}

void Puzel::setPosition(int ax, int ay){
    if (cx == ax && cy == ay){
        this->setCorrect(true);
    } else {
        this->setCorrect(false);
    }
}
