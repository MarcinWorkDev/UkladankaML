#ifndef PUZEL_H
#define PUZEL_H

/**
 *  Klasa odpowiedzialna za logikę obiektu Puzel. Klasa przechowuje informację o położeniu prawidłowym puzla oraz jego stan.
 */
class Puzel
{
public:
    /**
     *  Konstruktor klasy.
     * @param cx prawidłowa wartość współrzędnej X
     * @param cy prawidłowa wartość współrzędnej Y
     * @param v wartość puzla (kolejny numer na planszy)
     */
    Puzel(int cx, int cy, int v); // dane wejsciowe - pozycja prawidlowa i numer puzla
    /**
     *  Atrybut informujący o tym czy puzel jest na prawidłowej (true) czy błędnej (false) pozycji
     */
    bool isCorrect; // flaga czy na poprawnej pozycji
    /**
     *  Atrybut informujący o tym czy puzel jest pusty (jest puzlem "wyjętym" z układanki)
     */
    bool isEmpty;
    /**
     *  Atrybut określający prawidłową pozycję puzla w osi X
     */
    int cx; // prawidłowa pozycja X
    /**
     *  Atrybut określający prawidłową pozycję puzla w osi Y
     */
    int cy; // prawidłowa pozycja Y
    /**
     *  Atrybut określający wartość liczbową puzla
     */
    int v; // wartość puzla
    /**
     *  Metoda ustawiająca atrybut isCorrect puzla
     * @param isCorrect określa czy puzel jest na prawidłowej pozycji w układzie
     */
    void setCorrect(bool); // sprawdzenie czy puzel jest na prawidlowej pozycji
    /**
     *  Metoda ustawiająca atrybut isEmpty puzla
     * @param isEmpty określa czy puzel jest puzlem "wyjętym" z układanki
     */
    void setEmpty(bool); // ustawienie że puzla nie ma (ostatni)
    /**
     *  Metoda ustawiająca współrzędne X i Y prawidłowej pozycji puzla
     * @param ax współrzędna w osi X
     * @param ay współrzędna w osi Y
     */
    void setPosition(int ax, int ay);
};

#endif // PUZEL_H
