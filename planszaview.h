#ifndef PLANSZAVIEW_H
#define PLANSZAVIEW_H

#include <QGridLayout>
#include "plansza.h"

/**
 *  Klasa warstwy widoku planszy. Klasa odpowiada za komunikację między oknem głównym aplikacji a wartstą logiczną aplikacji.
 */
class PlanszaView : public QGridLayout
{
public:
    /**
     *  Konstruktor klasy
     * @param x wymiar (liczba elementów) w osi X
     * @param y wymiar (liczba elementów) w osi Y
     * @param level poziom trudności planszy
     */
    PlanszaView(int x, int y, int level);
    /**
     *  Odwołanie do klasy logicznej Planszy
     */
    Plansza plansza;
    /**
     *  Metoda rysująca planszę (generuje layout planszy)
     */
    void draw();
    /**
     *  Metoda przerysowująca planszę (aktualizuje aktualny layout planszy)
     */
    void redraw();
    /**
     *  Metoda przekazująca informację o dokonaniu ruchu do wartstwy logicznej oraz wykonująca ruch wizualny w warstwie widoku.
     * @param key wartość enum naciśniętego klawisza
     */
    void move(Qt::Key key);
    /**
     *  Metoda przekazująca informację o cofnięciu ruchu do warstawy logicznej oraz wykonująca ruch wizualny w warstwie widoku.
     */
    void undoMove();
    /**
     *  Metoda powodująca rozwiązanie układu w formie wizualnej (widoczny każdy ruch)
     */
    void solve();

private:
    int x;
    int y;
    int blankX;
    int blankY;
    int level;
    void buildLayout();
    void findBlank();
    bool checkGame();
    void delay(int millisecondsToWait);
};

#endif // PLANSZAVIEW_H
