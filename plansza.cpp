#include "plansza.h"
#include <random>
#include <chrono>

Plansza::Plansza()
{
}

Plansza::Plansza(int x, int y, int level)
{

    this->x = x;
    this->y = y;
    this->level = level;

    this->build();
    this->show();
    this->random();
    this->show();

}

void Plansza::build()
{

    int v = 0;

    try
    {
        for (auto a = 0; a <= y-1; a++)
        {
            std::vector<Puzel> row;
            for (auto aa = 0; aa <= x-1; aa++)
            {
                ++v;
                row.push_back(Puzel(a,aa,v));
            }
            puzzles.push_back(row);
        }

        puzzles[y-1][x-1].setEmpty(true);
    } catch (const std::exception e){
        std::cout<<e.what();
    }

}

bool Plansza::movePuzel(int fromX, int fromY, int toX, int toY)
{
    bool isUserMove = false;
    return this->movePuzel(fromX, fromY, toX, toY, isUserMove);
}

bool Plansza::movePuzel(int fromX, int fromY, int toX, int toY, bool isUserMove)
{

    moveData movedata = {
        fromX,
        fromY,
        toX,
        toY,
        true
    };

    if (!this->validMove(movedata))
    {
        std::cout<<"Ruch poza zakresem planszy!\n";
        return false;
    }

    Puzel from = puzzles[fromY][fromX];
    Puzel to = puzzles[toY][toX];

    if (to.isEmpty && abs(fromX-toX)<=1 && abs(fromY-toY)<=1){
        puzzles[fromY][fromX] = to;
        puzzles[toY][toX] = from;
        movesHistory.push_back(moveData { fromX, fromY, toX, toY, isUserMove });
        puzzles[toY][toX].setPosition(toY, toX);
        std::cout<<"Przesuniety puzel\n";
        this->show();
        return true;
    } else {
        std::cout<<"Nie mozna przesunac puzla\n";
        return false;
    }

}

bool Plansza::validMove(moveData move)
{
    int minX = 0;
    int minY = 0;
    int maxX = puzzles[0].size()-1;
    int maxY = puzzles.size()-1;

    if (move.fromX < minX || move.fromY < minY) {
        return false;
    } else if (move.fromX > maxX || move.fromY > maxY) {
        return false;
    } else if (move.toX < minX || move.toY < minY) {
        return false;
    } else if (move.toX > maxX || move.toY > maxY) {
        return false;
    } else {
        return true;
    }
}

void Plansza::undoMove()
{
    this->undoMove(true, 1);
}

void Plansza::undoMove(bool isUserMove, int moves)
{

    for (auto i=1 ; i <= moves ; ++i)
    {
        if (movesHistory.size() == 0) break;
        moveData lastMove = movesHistory[movesHistory.size()-1];
        movePuzel(lastMove.toX, lastMove.toY, lastMove.fromX, lastMove.fromY, isUserMove);
        movesHistory.pop_back();
        movesHistory.pop_back();
    }

}

void Plansza::show()
{
        std::cout<<"Uklad planszy:"<<std::endl;

        for (signed int i = 0; i < puzzles.size(); i++){
            std::string row = "";
            for (signed int ii = 0; ii < puzzles[i].size(); ii++){
                std::string empty = puzzles[i][ii].isEmpty?" ":std::to_string(puzzles[i][ii].v);
                std::string correct = puzzles[i][ii].isCorrect?"":"x";
                row     += "["
                        + empty
                        + correct
                        + "] ";
            }
            std::cout<<row<<std::endl;
        }

        std::cout<<"Wykonane ruchy: "<<movesHistory.size()<<std::endl<<std::endl;

}

void Plansza::random()
{
    int moves = this->level * level * 10;

    int blankY = puzzles.size() - 1;
    int blankX = puzzles[0].size() - 1;

    double min = 1;
    double max = 4;

    auto seed = std::chrono::high_resolution_clock::now().time_since_epoch().count();
    std::mt19937 e1(seed);
    std::uniform_int_distribution<> dist(min, max);

    int direction;
    int movesDone;
    moveData movek;

    while (movesDone <= moves)
    {
        direction = dist(e1);

        switch (direction)
        {
        case 1:
            movek = { blankX , blankY + 1 , blankX , blankY, false };
            break;
        case 2:
            movek = { blankX , blankY - 1 , blankX , blankY, false };
            break;
        case 3:
            movek = { blankX + 1 , blankY , blankX , blankY, false };
            break;
        case 4:
            movek = { blankX - 1 , blankY , blankX , blankY, false };
            break;
        }

        if (validMove(movek)){
            std::cout<<movek.fromX<<" "<<movek.toX<<" "<<movek.fromY<<" "<<movek.toY<<" "<<std::endl;

            movePuzel(movek.fromX, movek.fromY, movek.toX, movek.toY);
            blankX = movek.fromX;
            blankY = movek.fromY;
            movesDone++;
        }
    }

}

void Plansza::solveFast()
{
    this->undoMove(false,movesHistory.size());
}
