#ifndef PUZELVIEW_H
#define PUZELVIEW_H

#include <QWidget>
#include <QLCDNumber>
#include "ui_mainwindow.h"

/**
 *  Klasa odpowiedzialna za wartstę widoku puzla. Odpowiada za komunikację między warstwą widoku a wartstą logiki obiektu Puzel.
 */
class PuzelView : public QLCDNumber
{
public:
    /**
     *  Konstruktor
     */
    PuzelView();
    /**
     *  Konstruktor z określeniem wartości puzla oraz poprawności jego pozycji
     * @param v wartość puzla
     * @param isCorrect informacja o poprawnej (true) lub błędnej (false) pozycji puzla
     */
    PuzelView(int v, bool isCorrect);
    /**
     *  Metoda powodująca zmianę statusu poprawności pozycji puzla w układzie planszy
     * @param isCorrect informacja o poprawnej (true) lub błędnej (false) pozycji puzla
     */
    void setStatus(bool);
};

#endif // PUZELVIEW_H
