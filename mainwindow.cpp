#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QMessageBox>
#include <iostream>
#include "planszaview.h"
#include "plansza.h"
#include "puzelview.h"
#include <QProcess>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::keyPressEvent(QKeyEvent *event)
{
    if (ui->losujUkladBtn->isEnabled() == false)
    {
        this->planszaview->move((Qt::Key)event->key());
    }
}

void MainWindow::on_btnExit_clicked()
{
    MainWindow::close();
    QApplication::quit();
}

void MainWindow::on_losujUkladBtn_clicked()
{
    ui->sizeGroupBox->setEnabled(false);
    ui->levelGroupBox->setEnabled(false);
    ui->losujUkladBtn->setEnabled(false);
    ui->cofnijRuchBtn->setEnabled(true);
    ui->autoSolveBtn->setEnabled(true);

    int sizeX = ui->widthValue->value();
    int sizeY = ui->heightValue->value();
    int level = ui->levelValue->value();

    this->planszaview = new PlanszaView(sizeX,sizeY,level);

    ui->ukladPlanszy->addLayout(this->planszaview,0,0);
}

void MainWindow::on_cofnijRuchBtn_clicked()
{
    this->planszaview->undoMove();
}

void MainWindow::on_restartBtn_clicked()
{
    qApp->quit();
    QProcess::startDetached(qApp->arguments()[0], qApp->arguments());
}

void MainWindow::on_autoSolveBtn_clicked()
{
    this->planszaview->solve();
}
