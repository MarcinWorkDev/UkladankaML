#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QKeyEvent>
#include "planszaview.h"
#include "plansza.h"

namespace Ui {
/**
 *  Klasa okna głównego aplikacji. Klasa stanowi warstwę widoku.
 */
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    PlanszaView * planszaview;

private slots:
    void on_btnExit_clicked();

    void on_losujUkladBtn_clicked();

    void on_cofnijRuchBtn_clicked();

    void on_restartBtn_clicked();

    void on_autoSolveBtn_clicked();

private:
    Ui::MainWindow *ui;

protected:
    void keyPressEvent(QKeyEvent* event);

};

#endif // MAINWINDOW_H
